﻿using AutoMapper;
using ExamData.Application.ExamDatas.Commands.Requests;
using ExamData.Application.ExamDatas.Results;
using ExamData.Domain.Exams;
using System;
using System.Collections.Generic;
using System.Text;

namespace ExamData.Infrastructure.Mapping
{
    public class ExamProfile : Profile
    {
        public ExamProfile()
        {

            CreateMap<SetupExam, ExamResult>();
        }
    }
}
