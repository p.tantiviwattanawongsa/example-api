﻿using ExamData.Application.ExamDatas.Repositories;
using ExamData.Domain;
using ExamData.Domain.Exams;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ExamData.Infrastructure.Repositories
{
    public class ExamRepository : IExamReadOnlyRepository, IExamWriteOnlyRepository
    {
        public async Task<SetupExam> GetExamDataList()
        {
            var dataId = Guid.NewGuid().ToString("D");

            SetupExam setupExams = new SetupExam(
                Guid.NewGuid().ToString("D"),
                Guid.NewGuid().ToString("D"),
                Guid.NewGuid().ToString("D"),
                Guid.NewGuid().ToString("D"),
                Guid.NewGuid().ToString("D"),
                Guid.NewGuid().ToString("D")
            );


            return await Task.FromResult(setupExams);
        }
    }
}
