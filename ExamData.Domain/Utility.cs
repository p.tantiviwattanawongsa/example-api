﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace ExamData.Domain
{
    public class Utility
    {
        public static string DatetimeToStringBEShortDate(DateTime? date)
        {
            try
            {
                if (date != null)
                {
                    CultureInfo culture = new CultureInfo("th-TH");
                    return date.Value.ToString(culture.DateTimeFormat.ShortDatePattern, culture);
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static string DatetimeToStringBELongDate(DateTime? date)
        {
            try
            {
                if (date != null)
                {
                    CultureInfo culture = new CultureInfo("th-TH");
                    return date.Value.ToString(culture.DateTimeFormat.LongDatePattern, culture);
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string DateTimeToString(DateTime? date)
        {
            try
            {
                if (date != null)
                {
                    return date.Value.ToString("dd/MM/yyyy", new CultureInfo("en-US"));
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string DateTimeToStringBE(DateTime? date)
        {
            try
            {
                if (date != null)
                {
                    return date.Value.ToString("dd/MM/yyyy", new CultureInfo("th-TH"));
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string DateTimeToStringTimeFormat(DateTime? date)
        {
            try
            {
                if (date != null)
                {
                    return date.Value.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string DateTimeToStringBETimeFormat(DateTime? date)
        {
            try
            {
                if (date != null)
                {
                    return date.Value.ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("th-TH"));
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static DateTime? StringToDateTime(string value)
        {
            DateTime? convertDate;
            try
            {
                convertDate = Convert.ToDateTime(value);
            }
            catch (Exception)
            {
                convertDate = null;
            }
            return convertDate;
        }

        public static DateTime? StringBEToDateTime(string value)
        {
            DateTime? convertDate;
            try
            {
                convertDate = DateTime.Parse(value, new CultureInfo("th-TH"));
            }
            catch (Exception)
            {
                convertDate = null;
            }
            return convertDate;
        }

        public static decimal? StringToDecimal(string value)
        {
            Decimal? number;
            try
            {
                number = Decimal.Parse(value);
            }
            catch
            {
                number = null;
            }
            return number;
        }

        public static long? StringToLongNull(string value)
        {
            try
            {
                long? number = long.Parse(value);
                return number;
            }
            catch
            {
                return null;
            }
        }

        public static long StringToLong(string value)
        {
            try
            {
                long number = long.Parse(value);
                return number;
            }
            catch
            {
                return 0;
            }

        }

        public static DateTime? ObjectToDatetime(object value)
        {

            try
            {
                DateTime? convertDate = (DateTime)value.GetType().GetProperty("Value").GetValue(value);
                return convertDate;
            }
            catch (Exception)
            {
                return null;
            }

        }

        public static List<decimal?> ObjecOracleDecimaltToListDecimalNull(OracleParameter values)
        {
            try
            {
                List<decimal?> result = new List<decimal?>();
                IEnumerable enumerable = values.Value as IEnumerable;
                foreach (object element in enumerable)
                {
                    var elementOracleDecimal = (OracleDecimal)element;
                    if (elementOracleDecimal.IsNull)
                    {
                        result.Add(null);
                    }
                    else
                    {
                        result.Add(elementOracleDecimal.Value);
                    }
                }
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<string> ObjecOracleStringToListString(OracleParameter values)
        {
            try
            {
                List<string> result = new List<string>();
                IEnumerable enumerable = values.Value as IEnumerable;
                foreach (object element in enumerable)
                {
                    var elementOracleDecimal = (OracleString)element;
                    result.Add(elementOracleDecimal.Value);
                }
                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
