﻿using System;
using System.Net;

namespace ExamData.Domain
{
    public class ApiCustomException : Exception
    {
        /// <summary>
        /// Custom Exception เพื่อให้สะดวกในการ return HttpsStatusCode ต่างๆ
        /// </summary>
        /// <param name="httpStatusCode">HttpStatusCode ที่ต้องการ return</param>
        /// <param name="code">Error code ที่ต้องการระบุ ถ้ายังไม่ได้กำหนด Error code ใส่ "1"</param>
        /// <param name="msg">Error Message แสดงให้หน้า UI ให้ User เห็น </param>
        /// <param name="msgDev">Error Message ให้ Develper เห็น</param>
        /// <param name="description">Error Description ใส่ StackTrace หรือรายละเอียดอื่นๆ ยาวๆ</param>
        public ApiCustomException(HttpStatusCode httpStatusCode, string code, string msg, string msgDev, string description)
        {
            HttpStatusCode = httpStatusCode;
            Code = code;
            Msg = msg;
            MsgDev = msgDev;
            Description = description;
        }

        // http status code ที่ต้องการให้ API return
        public HttpStatusCode HttpStatusCode { get; private set; }

        // Error Code
        // 0 error แบบไม่ได้ดักไว้
        // 1 error แบบดักเอาไว้
        // อื่นๆ แล้วแต่ตั้งเอา
        public string Code { get; private set; }

        // Error Message แสดงให้หน้า UI ให้ User เห็น 
        public string Msg { get; private set; }

        // Error Message ให้ Develper เห็น
        public string MsgDev { get; private set; }

        // Error Description ใส่ StackTrace หรือรายละเอียดอื่นๆ ยาวๆ
        public string Description { get; private set; }
    }
}
