﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamData.Application.Model
{
    public class UserAuthenInfoRequest
    {
        public UserAuthenInfoRequest(string username, string fullName, string name, string surname, List<string> roles)
        {
            Username = username;
            FullName = fullName;
            Name = name;
            Surname = surname;
            Roles = roles;
        }

        public string Username { get; set; }
        public string FullName { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public List<string> Roles { get; set; }
    }
}
