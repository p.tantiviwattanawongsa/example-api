﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamData.Application.ExamDatas.Results
{
    public class ExamResult
    {
        public ExamResult(string dataId_1, string dataId_2, string dataId_3, string dataId_4, string dataId_5, string dataId_6)
        {
            this.dataId_1 = dataId_1;
            this.dataId_2 = dataId_2;
            this.dataId_3 = dataId_3;
            this.dataId_4 = dataId_4;
            this.dataId_5 = dataId_5;
            this.dataId_6 = dataId_6;
        }

        public string dataId_1 { get; private set; }
        public string dataId_2 { get; private set; }
        public string dataId_3 { get; private set; }
        public string dataId_4 { get; private set; }
        public string dataId_5 { get; private set; }
        public string dataId_6 { get; private set; }
    }
}
