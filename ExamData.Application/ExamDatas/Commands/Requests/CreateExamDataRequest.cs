﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExamData.Application.ExamDatas.Commands.Requests
{
    public class CreateExamDataRequest
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedNote { get; set; }
        public string Email { get; set; }
        public string Tel { get; set; }
        public string ActiveFlag { get; set; }
        public string Remark { get; set; }
    }
}
