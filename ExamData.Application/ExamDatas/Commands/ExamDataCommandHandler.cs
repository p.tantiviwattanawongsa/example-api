﻿using AutoMapper;
using ExamData.Application.ExamDatas.Repositories;
using ExamData.Application.ExamDatas.Results;
using ExamData.Domain;
using ExamData.Domain.Exams;
using MediatR;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExamData.Application.ExamDatas.Commands
{
    public class ExamDataCommandHandler
    {
        private readonly IExamReadOnlyRepository _examReadOnlyRepository;
        private readonly IExamWriteOnlyRepository _examWriteOnlyRepository;
        private readonly IMapper _mapper;

        public ExamDataCommandHandler(IExamReadOnlyRepository examReadOnlyRepository, IExamWriteOnlyRepository examWriteOnlyRepository, IMapper mapper)
        {
            _examReadOnlyRepository = examReadOnlyRepository;
            _examWriteOnlyRepository = examWriteOnlyRepository;
            _mapper = mapper;
        }

    }
}
