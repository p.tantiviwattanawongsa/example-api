﻿using AutoMapper;
using ExamData.Application.ExamDatas.Repositories;
using ExamData.Application.ExamDatas.Results;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ExamData.Application.ExamDatas.Querises
{
    public class ExamQueryHandler : IRequestHandler<GetExamListQuery, ExamResult>
    {

        private readonly IExamReadOnlyRepository _examReadOnlyRepository;
        private readonly IExamWriteOnlyRepository _examWriteOnlyRepository;
        private readonly IMapper _mapper;

        public ExamQueryHandler(IExamReadOnlyRepository examReadOnlyRepository, IExamWriteOnlyRepository examWriteOnlyRepository, IMapper mapper)
        {
            _examReadOnlyRepository = examReadOnlyRepository;
            _examWriteOnlyRepository = examWriteOnlyRepository;
            _mapper = mapper;
        }

        public async Task<ExamResult> Handle(GetExamListQuery request, CancellationToken cancellationToken)
        {

            var examResult = await _examReadOnlyRepository.GetExamDataList();
            var result = _mapper.Map<ExamResult>(examResult);
            return result;

        }
    }
}
