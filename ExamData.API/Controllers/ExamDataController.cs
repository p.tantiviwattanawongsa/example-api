﻿using ExamData.Application.ExamDatas.Commands;
using ExamData.Application.ExamDatas.Commands.Requests;
using ExamData.Application.ExamDatas.Querises;
using ExamData.Application.ExamDatas.Results;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ExamData.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExamDataController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ExamDataController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        [Route("GetListDataExample")]
        public async Task<ExamResult> ExamDataList()
        {
            var result = await _mediator.Send(new GetExamListQuery());
            return result;
        }
    }
}
