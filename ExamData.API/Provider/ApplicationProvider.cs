﻿//using BPayMonitor.Application.Model;
//using BPayMonitor.Domain;
//using IdentityModel.Client;
using ExamData.Application.Model;
using ExamData.Domain;
using IdentityModel.Client;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BPayMonitor.API.Provider
{
    public class ApplicationProvider
    {
        public static async Task<UserAuthenInfoRequest> GetUserInfoAuthen(HttpContext httpContext, string ids4UserInfoEndpoint)
        {
            if (httpContext.User.Identity.AuthenticationType == "BearerIdentityServerAuthenticationIntrospection")
            {
                var accessToken = RemoveBearer(httpContext.Request.Headers[HeaderNames.Authorization].ToString());
                return await GetUserFromIds4Token(ids4UserInfoEndpoint, accessToken);
            }

            throw new ApiCustomException(HttpStatusCode.Unauthorized, "1", "UnauthorizedAccess", "Invalid AuthenticationType", null);
        }


        public static async Task<UserAuthenInfoRequest> GetUserFromIds4Token(string ids4UserInfoEndpoint, string accessToken)
        {
            using (HttpClient client = new HttpClient())
            {
                var response = await client.GetUserInfoAsync(new UserInfoRequest
                {
                    Address = ids4UserInfoEndpoint,
                    Token = accessToken
                });

                var claims = response.Claims.ToList();

                var userType = GetValueOrNull(claims.Where(x => x.Type == "UserType").FirstOrDefault());
                var userId = GetValueOrNull(claims.Where(x => x.Type == "preferred_username").FirstOrDefault());
                var username = GetValueOrNull(claims.Where(x => x.Type == "preferred_username").FirstOrDefault());
                var titleName = GetValueOrNull(claims.Where(x => x.Type == "name").FirstOrDefault());

                UserAuthenInfoRequest userAuthenInfo = new UserAuthenInfoRequest(
                    username,
                    titleName,
                    null,
                    null,
                    null
                );

                return await Task.FromResult(userAuthenInfo);
            }
        }


        public static async Task<UserAuthenInfoRequest> GetUserFromIsisOauth2Token(HttpContext httpContext)
        {
            IEnumerable<Claim> claims = httpContext.User.Claims;
            var username = GetValueOrNull(claims.Where(x => x.Type == ClaimTypes.Name).FirstOrDefault());
            var name = GetValueOrNull(claims.Where(x => x.Type == ClaimTypes.GivenName).FirstOrDefault());
            var surname = GetValueOrNull(claims.Where(x => x.Type == ClaimTypes.Surname).FirstOrDefault());
            var rolesFromToken = claims.Where(x => x.Type == ClaimTypes.Role).ToList();
            List<string> roles = new List<string>();
            foreach (var i in rolesFromToken)
            {
                roles.Add(i.Value);
            }

            UserAuthenInfoRequest userAuthenInfo = new UserAuthenInfoRequest(username, name + " " + surname, name, surname, roles);
            return await Task.FromResult(userAuthenInfo);
        }

        public static string GetValueOrNull(System.Security.Claims.Claim claim)
        {
            if (claim != null)
            {
                return claim.Value;
            }
            return null;
        }

        public static string RemoveBearer(string token)
        {
            return token.Replace("Bearer ", "").Replace("bearer", "");
        }
    }
}
