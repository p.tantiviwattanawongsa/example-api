﻿using Autofac;
using ExamData.Application.ExamDatas.Repositories;
using ExamData.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamData.API
{
    public class AutofacModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // The generic ILogger<TCategoryName> service was added to the ServiceCollection by ASP.NET Core.
            // It was then registered with Autofac using the Populate method. All of this starts
            // with the `UseServiceProviderFactory(new AutofacServiceProviderFactory())` that happens in Program and registers Autofac
            // as the service provider.

            builder.RegisterType<ExamRepository>()
                .As<IExamReadOnlyRepository>()
                .As<IExamWriteOnlyRepository>()
                .InstancePerLifetimeScope();

        }

    }
}
