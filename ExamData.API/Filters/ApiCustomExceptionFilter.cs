﻿using ExamData.Domain;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace BPayMonitor.API.Filters
{
    public class ApiCustomExceptionFilter : IExceptionFilter
    {
        private bool ShowErrorDetail = false;

        public ApiCustomExceptionFilter(IConfiguration configuration)
        {
            ShowErrorDetail = bool.Parse(configuration["Error:ShowErrorDetail"]);
        }

        public void OnException(ExceptionContext context)
        {
            ApiCustomException apiCustomException = context.Exception as ApiCustomException;
            if (apiCustomException != null)    // from manual throw Exception
            {
                ErrorObjectResult errorObjectResult = new ErrorObjectResult(
                    apiCustomException.Code,
                    apiCustomException.Msg,
                    ShowErrorDetail ? apiCustomException.MsgDev : null,
                    ShowErrorDetail ? apiCustomException.Description : null
                );

                context.Result = new ObjectResult(errorObjectResult);
                context.HttpContext.Response.StatusCode = (int)apiCustomException.HttpStatusCode;
            }
            else if (context.Exception.InnerException != null)  // from another throw Exception with InnterException
            {
                while (context.Exception.InnerException != null)
                {
                    context.Exception = context.Exception.InnerException;
                }

                ErrorObjectResult errorObjectResult = new ErrorObjectResult(
                    "0",
                    "An error occurred.",
                    ShowErrorDetail ? context.Exception.Message : null,
                    ShowErrorDetail ? context.Exception.StackTrace : null
                );

                context.Result = new ObjectResult(errorObjectResult);
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
            else if (context.Exception != null)                  // from another throw Exception
            {
                ErrorObjectResult errorObjectResult = new ErrorObjectResult(
                    "0",
                    "An error occurred.",
                    ShowErrorDetail ? context.Exception.Message : null,
                    ShowErrorDetail ? context.Exception.StackTrace : null
                );

                context.Result = new ObjectResult(errorObjectResult);
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }
        }
    }

    public class ErrorObjectResult
    {
        public ErrorObjectResult(string code, string message, string messageDev, string description)
        {
            Code = code;
            Message = message;
            MessageDev = messageDev;
            Description = description;
        }

        // Error Code
        // 0 error แบบไม่ได้ดักไว้
        // 1 error แบบดักเอาไว้
        // อื่นๆ แล้วแต่ตั้งเอา
        public string Code { get; private set; }

        // Error Message แสดงให้หน้า UI ให้ User เห็น 
        public string Message { get; private set; }

        // Error Message ให้ Develper เห็น
        public string MessageDev { get; private set; }

        // Error Description ใส่ StackTrace หรือรายละเอียดอื่นๆยาวๆ
        public string Description { get; private set; }
    }
}
